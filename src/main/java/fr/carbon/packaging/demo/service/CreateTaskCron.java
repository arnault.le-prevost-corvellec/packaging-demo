package fr.carbon.packaging.demo.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import reactor.core.Disposable;
import reactor.core.publisher.Hooks;

@Component
public class CreateTaskCron {

    private static final Logger logger = LoggerFactory.getLogger(CreateTaskCron.class);
    private final TaskService service;
    private final List<String> words;
    private Random random;

    CreateTaskCron(TaskService service) throws IOException {
        Hooks.onOperatorDebug();
        this.service = service;
        this.words = loadFile("gutenberg.txt");
        this.random = new Random();
    }

    private List<String> loadFile(String string) throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader((CreateTaskCron.class.getClassLoader().getResourceAsStream(string))))) {
            String line;
            Set<String> result = new HashSet<>();
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            return new ArrayList<>(result);
        }
    }

    @Scheduled(fixedRateString = "${schedule.task-creation.fixe-rate-delay}")
    public Disposable createTask() {
        return service.createTask(generateRandomLabel())
                .doOnError(e -> logger.warn("exception during task creation : {}", e.getClass().getSimpleName()))
                .subscribe();
    }

    private String generateRandomLabel() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            sb.append(words.get(random.nextInt(words.size())));
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
