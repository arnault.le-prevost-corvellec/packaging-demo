package fr.carbon.packaging.demo.service;

import org.springframework.stereotype.Service;

import fr.carbon.packaging.demo.mapper.TaskMapper;
import fr.carbon.packaging.demo.repository.TaskEntity;
import fr.carbon.packaging.demo.repository.TaskRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TaskService {

    private final TaskRepository repository;
    private final TaskMapper mapper;

    public TaskService(TaskRepository repository, TaskMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public Flux<Task> getTasks() {
        return repository.findAll().map(mapper::toTask);
    }

    public Mono<Long> getTasksCount() {
        return repository.count();
    }

    public Mono<Task> createTask(String label) {
        return Mono.just(label)
                .map(this::createTaskEntity)
                .flatMap(repository::save)
                .map(mapper::toTask);
    }

    private TaskEntity createTaskEntity(String label) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setLabel(label);
        return taskEntity;
    }

    public Mono<Void> deleteAllTasks() {
        return repository.deleteAll();
    }

}
