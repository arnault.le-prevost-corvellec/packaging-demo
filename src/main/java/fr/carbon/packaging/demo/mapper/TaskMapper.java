package fr.carbon.packaging.demo.mapper;

import org.mapstruct.Mapper;

import fr.carbon.packaging.demo.api.dto.TaskDto;
import fr.carbon.packaging.demo.repository.TaskEntity;
import fr.carbon.packaging.demo.service.Task;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskDto toTaskDto(Task task);

    Task toTask(TaskEntity task);
}
