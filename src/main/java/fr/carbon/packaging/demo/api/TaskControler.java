package fr.carbon.packaging.demo.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.carbon.packaging.demo.api.dto.TaskDto;
import fr.carbon.packaging.demo.mapper.TaskMapper;
import fr.carbon.packaging.demo.service.TaskService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class TaskControler {

    private final TaskService service;
    private final TaskMapper mapper;

    TaskControler(TaskService service, TaskMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("tasks")
    Flux<TaskDto> getTasks() {
        return service.getTasks().map(mapper::toTaskDto);
    }

    @PostMapping("tasks")
    Mono<TaskDto> createTask(@RequestBody TaskDto label) {
        return service.createTask(label.label()).map(mapper::toTaskDto);
    }

    @GetMapping("tasks/count")
    Mono<Long> getTasksCount() {
        return service.getTasksCount();
    }

    @DeleteMapping("tasks")
    Mono<Void> deleteAllTasks() {
        return service.deleteAllTasks();
    }

}
