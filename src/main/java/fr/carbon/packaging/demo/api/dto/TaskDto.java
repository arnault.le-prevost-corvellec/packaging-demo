package fr.carbon.packaging.demo.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public record TaskDto(
        @Schema(name = "id", accessMode = Schema.AccessMode.READ_ONLY, requiredMode = Schema.RequiredMode.NOT_REQUIRED) Long id,
        String label) {

}
