package fr.carbon.packaging.demo.aot;

import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;

import fr.carbon.packaging.demo.api.dto.TaskDto;

public class CustomRuntimeHintsRegistrar implements RuntimeHintsRegistrar {

	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		hints.resources().registerPattern("gutenberg.txt");
		hints.reflection().registerType(TaskDto[].class);
	}

}