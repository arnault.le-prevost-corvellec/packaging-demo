package fr.carbon.packaging.demo.repository;

import org.springframework.data.annotation.Id;

public class TaskEntity {

    @Id
    private Long id;

    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
