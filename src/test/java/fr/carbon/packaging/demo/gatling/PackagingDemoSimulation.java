package fr.carbon.packaging.demo.gatling;

import static io.gatling.javaapi.core.CoreDsl.exec;
import static io.gatling.javaapi.core.CoreDsl.rampConcurrentUsers;
import static io.gatling.javaapi.core.CoreDsl.scenario;
import static io.gatling.javaapi.http.HttpDsl.http;
import static io.gatling.javaapi.http.HttpDsl.status;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import io.gatling.javaapi.core.ChainBuilder;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;
import io.gatling.javaapi.http.HttpRequestActionBuilder;

/**
 * Simulation Gatling Turf Info
 */
public class PackagingDemoSimulation extends Simulation {

        /**
         * Récupération des propriétes se trouvant dans le fichier gatling.conf
         */
        private final Config defaultConfig = ConfigFactory.parseResources("gatling.conf");

        private final String url = defaultConfig.getString("gatling.simulation.baseUrl");

        private final int timeWindow = defaultConfig.getInt("gatling.simulation.timeWindow");

        private final int maxUsers = defaultConfig.getInt("gatling.simulation.maxUsers");

        private final int minUsers = defaultConfig.getInt("gatling.simulation.minUsers");

        /**
         * Définition du protocole HTTP permettant de requêter l'API
         */
        private final HttpProtocolBuilder httpProtocol = http.baseUrl(url)
                        .acceptHeader("application/json;q=0.9,*/*;q=0.8")
                        // .acceptLanguageHeader("fr,en-US;q=0.7,en;q=0.3")
                        .userAgentHeader(
                                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/116.0");

        /**
         * Un chainBuilder permet de construire une chaine de process au sein d'un même
         * domaine métier
         * ie: Ici la chaine concerne uniquement le programme.
         */
        private final ChainBuilder getTasks = exec(checkStatus(http("Retrieve tasks")
                        .get("/tasks")));
        private final ChainBuilder createTasks = exec(checkStatus(http("Create task")
                        .post("/tasks").formParam("label", this::createBody)));

        /**
         * On définit ici les différents types de populations qui utiliseraient les
         * chainBuilder construits ci-dessus
         */

        private final ScenarioBuilder programInternetPersona = scenario("Retrieve and create")
                        .exec(getTasks
                        // , createTasks
                        );

        {
                /**
                 * Lancement de la simulation.
                 * La stratégie ici est d'injecter $usersPerSec user/sec de manière constante
                 * durant $timeWindow
                 */
                setUp(
                                programInternetPersona.injectClosed(
                                                rampConcurrentUsers(minUsers).to(maxUsers).during(timeWindow)))
                                .protocols(httpProtocol);
        }

        private HttpRequestActionBuilder checkStatus(HttpRequestActionBuilder httpRequestActionBuilder) {
                return httpRequestActionBuilder.check(
                                status()
                                                .is(session -> 200));
        }

        private int i = 0;

        private String createBody(io.gatling.javaapi.core.Session s) {
                return "task gatling n°" + i++;
        }
}
