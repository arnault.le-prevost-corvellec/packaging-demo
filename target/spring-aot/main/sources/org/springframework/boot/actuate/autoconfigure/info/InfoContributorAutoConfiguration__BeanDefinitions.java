package org.springframework.boot.actuate.autoconfigure.info;

import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.info.BuildProperties;

/**
 * Bean definitions for {@link InfoContributorAutoConfiguration}.
 */
public class InfoContributorAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean definition for 'infoContributorAutoConfiguration'.
   */
  public static BeanDefinition getInfoContributorAutoConfigurationBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(InfoContributorAutoConfiguration.class);
    beanDefinition.setInstanceSupplier(InfoContributorAutoConfiguration::new);
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'buildInfoContributor'.
   */
  private static BeanInstanceSupplier<InfoContributor> getBuildInfoContributorInstanceSupplier() {
    return BeanInstanceSupplier.<InfoContributor>forFactoryMethod(InfoContributorAutoConfiguration.class, "buildInfoContributor", BuildProperties.class)
            .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(InfoContributorAutoConfiguration.class).buildInfoContributor(args.get(0)));
  }

  /**
   * Get the bean definition for 'buildInfoContributor'.
   */
  public static BeanDefinition getBuildInfoContributorBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(InfoContributor.class);
    beanDefinition.setInstanceSupplier(getBuildInfoContributorInstanceSupplier());
    return beanDefinition;
  }
}
