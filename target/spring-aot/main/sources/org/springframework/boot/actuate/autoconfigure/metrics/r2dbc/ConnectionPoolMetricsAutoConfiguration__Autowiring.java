package org.springframework.boot.actuate.autoconfigure.metrics.r2dbc;

import io.micrometer.core.instrument.MeterRegistry;
import java.util.Map;
import org.springframework.beans.factory.aot.AutowiredMethodArgumentsResolver;
import org.springframework.beans.factory.support.RegisteredBean;

/**
 * Autowiring for {@link ConnectionPoolMetricsAutoConfiguration}.
 */
public class ConnectionPoolMetricsAutoConfiguration__Autowiring {
  /**
   * Apply the autowiring.
   */
  public static ConnectionPoolMetricsAutoConfiguration apply(RegisteredBean registeredBean,
      ConnectionPoolMetricsAutoConfiguration instance) {
    AutowiredMethodArgumentsResolver.forRequiredMethod("bindConnectionPoolsToRegistry", Map.class, MeterRegistry.class).resolve(registeredBean, args -> instance.bindConnectionPoolsToRegistry(args.get(0), args.get(1)));
    return instance;
  }
}
