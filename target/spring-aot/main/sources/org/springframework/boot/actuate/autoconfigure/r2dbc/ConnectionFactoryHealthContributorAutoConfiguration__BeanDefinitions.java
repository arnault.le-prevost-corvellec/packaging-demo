package org.springframework.boot.actuate.autoconfigure.r2dbc;

import java.util.Map;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.actuate.health.ReactiveHealthContributor;

/**
 * Bean definitions for {@link ConnectionFactoryHealthContributorAutoConfiguration}.
 */
public class ConnectionFactoryHealthContributorAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean instance supplier for 'org.springframework.boot.actuate.autoconfigure.r2dbc.ConnectionFactoryHealthContributorAutoConfiguration'.
   */
  private static BeanInstanceSupplier<ConnectionFactoryHealthContributorAutoConfiguration> getConnectionFactoryHealthContributorAutoConfigurationInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<ConnectionFactoryHealthContributorAutoConfiguration>forConstructor(Map.class)
            .withGenerator((registeredBean, args) -> new ConnectionFactoryHealthContributorAutoConfiguration(args.get(0)));
  }

  /**
   * Get the bean definition for 'connectionFactoryHealthContributorAutoConfiguration'.
   */
  public static BeanDefinition getConnectionFactoryHealthContributorAutoConfigurationBeanDefinition(
      ) {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(ConnectionFactoryHealthContributorAutoConfiguration.class);
    beanDefinition.setInstanceSupplier(getConnectionFactoryHealthContributorAutoConfigurationInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'r2dbcHealthContributor'.
   */
  private static BeanInstanceSupplier<ReactiveHealthContributor> getRdbcHealthContributorInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<ReactiveHealthContributor>forFactoryMethod(ConnectionFactoryHealthContributorAutoConfiguration.class, "r2dbcHealthContributor")
            .withGenerator((registeredBean) -> registeredBean.getBeanFactory().getBean(ConnectionFactoryHealthContributorAutoConfiguration.class).r2dbcHealthContributor());
  }

  /**
   * Get the bean definition for 'r2dbcHealthContributor'.
   */
  public static BeanDefinition getRdbcHealthContributorBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(ReactiveHealthContributor.class);
    beanDefinition.setInstanceSupplier(getRdbcHealthContributorInstanceSupplier());
    return beanDefinition;
  }
}
