package org.springframework.boot.actuate.autoconfigure.metrics.r2dbc;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.InstanceSupplier;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * Bean definitions for {@link ConnectionPoolMetricsAutoConfiguration}.
 */
public class ConnectionPoolMetricsAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean definition for 'connectionPoolMetricsAutoConfiguration'.
   */
  public static BeanDefinition getConnectionPoolMetricsAutoConfigurationBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(ConnectionPoolMetricsAutoConfiguration.class);
    InstanceSupplier<ConnectionPoolMetricsAutoConfiguration> instanceSupplier = InstanceSupplier.using(ConnectionPoolMetricsAutoConfiguration::new);
    instanceSupplier = instanceSupplier.andThen(ConnectionPoolMetricsAutoConfiguration__Autowiring::apply);
    beanDefinition.setInstanceSupplier(instanceSupplier);
    return beanDefinition;
  }
}
