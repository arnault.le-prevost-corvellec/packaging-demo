package org.springframework.boot.actuate.autoconfigure.observation.web.reactive;

import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.observation.ObservationRegistry;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties;
import org.springframework.boot.actuate.autoconfigure.observation.ObservationProperties;
import org.springframework.web.filter.reactive.ServerHttpObservationFilter;

/**
 * Bean definitions for {@link WebFluxObservationAutoConfiguration}.
 */
public class WebFluxObservationAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean instance supplier for 'org.springframework.boot.actuate.autoconfigure.observation.web.reactive.WebFluxObservationAutoConfiguration'.
   */
  private static BeanInstanceSupplier<WebFluxObservationAutoConfiguration> getWebFluxObservationAutoConfigurationInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<WebFluxObservationAutoConfiguration>forConstructor(MetricsProperties.class, ObservationProperties.class)
            .withGenerator((registeredBean, args) -> new WebFluxObservationAutoConfiguration(args.get(0), args.get(1)));
  }

  /**
   * Get the bean definition for 'webFluxObservationAutoConfiguration'.
   */
  public static BeanDefinition getWebFluxObservationAutoConfigurationBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(WebFluxObservationAutoConfiguration.class);
    beanDefinition.setInstanceSupplier(getWebFluxObservationAutoConfigurationInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'webfluxObservationFilter'.
   */
  private static BeanInstanceSupplier<ServerHttpObservationFilter> getWebfluxObservationFilterInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<ServerHttpObservationFilter>forFactoryMethod(WebFluxObservationAutoConfiguration.class, "webfluxObservationFilter", ObservationRegistry.class, ObjectProvider.class, ObjectProvider.class, ObjectProvider.class)
            .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(WebFluxObservationAutoConfiguration.class).webfluxObservationFilter(args.get(0), args.get(1), args.get(2), args.get(3)));
  }

  /**
   * Get the bean definition for 'webfluxObservationFilter'.
   */
  public static BeanDefinition getWebfluxObservationFilterBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(ServerHttpObservationFilter.class);
    beanDefinition.setInstanceSupplier(getWebfluxObservationFilterInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Bean definitions for {@link WebFluxObservationAutoConfiguration.MeterFilterConfiguration}.
   */
  public static class MeterFilterConfiguration {
    /**
     * Get the bean definition for 'meterFilterConfiguration'.
     */
    public static BeanDefinition getMeterFilterConfigurationBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition(WebFluxObservationAutoConfiguration.MeterFilterConfiguration.class);
      beanDefinition.setInstanceSupplier(WebFluxObservationAutoConfiguration.MeterFilterConfiguration::new);
      return beanDefinition;
    }

    /**
     * Get the bean instance supplier for 'metricsHttpServerUriTagFilter'.
     */
    private static BeanInstanceSupplier<MeterFilter> getMetricsHttpServerUriTagFilterInstanceSupplier(
        ) {
      return BeanInstanceSupplier.<MeterFilter>forFactoryMethod(WebFluxObservationAutoConfiguration.MeterFilterConfiguration.class, "metricsHttpServerUriTagFilter", MetricsProperties.class, ObservationProperties.class)
              .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(WebFluxObservationAutoConfiguration.MeterFilterConfiguration.class).metricsHttpServerUriTagFilter(args.get(0), args.get(1)));
    }

    /**
     * Get the bean definition for 'metricsHttpServerUriTagFilter'.
     */
    public static BeanDefinition getMetricsHttpServerUriTagFilterBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition();
      beanDefinition.setTargetType(MeterFilter.class);
      beanDefinition.setInstanceSupplier(getMetricsHttpServerUriTagFilterInstanceSupplier());
      return beanDefinition;
    }
  }
}
