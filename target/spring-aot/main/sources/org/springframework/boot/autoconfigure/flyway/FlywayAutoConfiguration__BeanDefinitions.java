package org.springframework.boot.autoconfigure.flyway;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.io.ResourceLoader;

/**
 * Bean definitions for {@link FlywayAutoConfiguration}.
 */
public class FlywayAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean definition for 'flywayAutoConfiguration'.
   */
  public static BeanDefinition getFlywayAutoConfigurationBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(FlywayAutoConfiguration.class);
    beanDefinition.setInstanceSupplier(FlywayAutoConfiguration::new);
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'stringOrNumberMigrationVersionConverter'.
   */
  private static BeanInstanceSupplier<FlywayAutoConfiguration.StringOrNumberToMigrationVersionConverter> getStringOrNumberMigrationVersionConverterInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<FlywayAutoConfiguration.StringOrNumberToMigrationVersionConverter>forFactoryMethod(FlywayAutoConfiguration.class, "stringOrNumberMigrationVersionConverter")
            .withGenerator((registeredBean) -> registeredBean.getBeanFactory().getBean(FlywayAutoConfiguration.class).stringOrNumberMigrationVersionConverter());
  }

  /**
   * Get the bean definition for 'stringOrNumberMigrationVersionConverter'.
   */
  public static BeanDefinition getStringOrNumberMigrationVersionConverterBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(FlywayAutoConfiguration.StringOrNumberToMigrationVersionConverter.class);
    beanDefinition.setInstanceSupplier(getStringOrNumberMigrationVersionConverterInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'flywayDefaultDdlModeProvider'.
   */
  private static BeanInstanceSupplier<FlywaySchemaManagementProvider> getFlywayDefaultDdlModeProviderInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<FlywaySchemaManagementProvider>forFactoryMethod(FlywayAutoConfiguration.class, "flywayDefaultDdlModeProvider", ObjectProvider.class)
            .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(FlywayAutoConfiguration.class).flywayDefaultDdlModeProvider(args.get(0)));
  }

  /**
   * Get the bean definition for 'flywayDefaultDdlModeProvider'.
   */
  public static BeanDefinition getFlywayDefaultDdlModeProviderBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(FlywaySchemaManagementProvider.class);
    beanDefinition.setInstanceSupplier(getFlywayDefaultDdlModeProviderInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Bean definitions for {@link FlywayAutoConfiguration.FlywayConfiguration}.
   */
  public static class FlywayConfiguration {
    /**
     * Get the bean definition for 'flywayConfiguration'.
     */
    public static BeanDefinition getFlywayConfigurationBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition(FlywayAutoConfiguration.FlywayConfiguration.class);
      beanDefinition.setInstanceSupplier(FlywayAutoConfiguration.FlywayConfiguration::new);
      return beanDefinition;
    }

    /**
     * Get the bean instance for 'resourceProviderCustomizer'.
     */
    private static NativeImageResourceProviderCustomizer getResourceProviderCustomizerInstance() {
      return new NativeImageResourceProviderCustomizer();
    }

    /**
     * Get the bean definition for 'resourceProviderCustomizer'.
     */
    public static BeanDefinition getResourceProviderCustomizerBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition();
      beanDefinition.setTargetType(ResourceProviderCustomizer.class);
      beanDefinition.setInstanceSupplier(FlywayConfiguration::getResourceProviderCustomizerInstance);
      return beanDefinition;
    }

    /**
     * Get the bean instance supplier for 'flywayConnectionDetails'.
     */
    private static BeanInstanceSupplier<FlywayAutoConfiguration.PropertiesFlywayConnectionDetails> getFlywayConnectionDetailsInstanceSupplier(
        ) {
      return BeanInstanceSupplier.<FlywayAutoConfiguration.PropertiesFlywayConnectionDetails>forFactoryMethod(FlywayAutoConfiguration.FlywayConfiguration.class, "flywayConnectionDetails", FlywayProperties.class)
              .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(FlywayAutoConfiguration.FlywayConfiguration.class).flywayConnectionDetails(args.get(0)));
    }

    /**
     * Get the bean definition for 'flywayConnectionDetails'.
     */
    public static BeanDefinition getFlywayConnectionDetailsBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition();
      beanDefinition.setTargetType(FlywayAutoConfiguration.PropertiesFlywayConnectionDetails.class);
      beanDefinition.setInstanceSupplier(getFlywayConnectionDetailsInstanceSupplier());
      return beanDefinition;
    }

    /**
     * Get the bean instance supplier for 'flyway'.
     */
    private static BeanInstanceSupplier<Flyway> getFlywayInstanceSupplier() {
      return BeanInstanceSupplier.<Flyway>forFactoryMethod(FlywayAutoConfiguration.FlywayConfiguration.class, "flyway", FlywayProperties.class, FlywayConnectionDetails.class, ResourceLoader.class, ObjectProvider.class, ObjectProvider.class, ObjectProvider.class, ObjectProvider.class, ObjectProvider.class, ResourceProviderCustomizer.class)
              .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(FlywayAutoConfiguration.FlywayConfiguration.class).flyway(args.get(0), args.get(1), args.get(2), args.get(3), args.get(4), args.get(5), args.get(6), args.get(7), args.get(8)));
    }

    /**
     * Get the bean definition for 'flyway'.
     */
    public static BeanDefinition getFlywayBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition();
      beanDefinition.setTargetType(Flyway.class);
      beanDefinition.setInstanceSupplier(getFlywayInstanceSupplier());
      return beanDefinition;
    }

    /**
     * Get the bean instance supplier for 'flywayInitializer'.
     */
    private static BeanInstanceSupplier<FlywayMigrationInitializer> getFlywayInitializerInstanceSupplier(
        ) {
      return BeanInstanceSupplier.<FlywayMigrationInitializer>forFactoryMethod(FlywayAutoConfiguration.FlywayConfiguration.class, "flywayInitializer", Flyway.class, ObjectProvider.class)
              .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(FlywayAutoConfiguration.FlywayConfiguration.class).flywayInitializer(args.get(0), args.get(1)));
    }

    /**
     * Get the bean definition for 'flywayInitializer'.
     */
    public static BeanDefinition getFlywayInitializerBeanDefinition() {
      RootBeanDefinition beanDefinition = new RootBeanDefinition();
      beanDefinition.setTargetType(FlywayMigrationInitializer.class);
      beanDefinition.setDependsOn("r2dbcScriptDatabaseInitializer");
      beanDefinition.setInstanceSupplier(getFlywayInitializerInstanceSupplier());
      return beanDefinition;
    }
  }
}
