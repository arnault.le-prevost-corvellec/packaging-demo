package org.springframework.boot.autoconfigure.task;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.LazyInitializationExcludeFilter;
import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Bean definitions for {@link TaskSchedulingAutoConfiguration}.
 */
public class TaskSchedulingAutoConfiguration__BeanDefinitions {
  /**
   * Get the bean definition for 'taskSchedulingAutoConfiguration'.
   */
  public static BeanDefinition getTaskSchedulingAutoConfigurationBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(TaskSchedulingAutoConfiguration.class);
    beanDefinition.setInstanceSupplier(TaskSchedulingAutoConfiguration::new);
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'taskScheduler'.
   */
  private static BeanInstanceSupplier<ThreadPoolTaskScheduler> getTaskSchedulerInstanceSupplier() {
    return BeanInstanceSupplier.<ThreadPoolTaskScheduler>forFactoryMethod(TaskSchedulingAutoConfiguration.class, "taskScheduler", TaskSchedulerBuilder.class)
            .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(TaskSchedulingAutoConfiguration.class).taskScheduler(args.get(0)));
  }

  /**
   * Get the bean definition for 'taskScheduler'.
   */
  public static BeanDefinition getTaskSchedulerBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(ThreadPoolTaskScheduler.class);
    beanDefinition.setInstanceSupplier(getTaskSchedulerInstanceSupplier());
    return beanDefinition;
  }

  /**
   * Get the bean definition for 'scheduledBeanLazyInitializationExcludeFilter'.
   */
  public static BeanDefinition getScheduledBeanLazyInitializationExcludeFilterBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(TaskSchedulingAutoConfiguration.class);
    beanDefinition.setTargetType(LazyInitializationExcludeFilter.class);
    beanDefinition.setInstanceSupplier(BeanInstanceSupplier.<LazyInitializationExcludeFilter>forFactoryMethod(TaskSchedulingAutoConfiguration.class, "scheduledBeanLazyInitializationExcludeFilter").withGenerator((registeredBean) -> TaskSchedulingAutoConfiguration.scheduledBeanLazyInitializationExcludeFilter()));
    return beanDefinition;
  }

  /**
   * Get the bean instance supplier for 'taskSchedulerBuilder'.
   */
  private static BeanInstanceSupplier<TaskSchedulerBuilder> getTaskSchedulerBuilderInstanceSupplier(
      ) {
    return BeanInstanceSupplier.<TaskSchedulerBuilder>forFactoryMethod(TaskSchedulingAutoConfiguration.class, "taskSchedulerBuilder", TaskSchedulingProperties.class, ObjectProvider.class)
            .withGenerator((registeredBean, args) -> registeredBean.getBeanFactory().getBean(TaskSchedulingAutoConfiguration.class).taskSchedulerBuilder(args.get(0), args.get(1)));
  }

  /**
   * Get the bean definition for 'taskSchedulerBuilder'.
   */
  public static BeanDefinition getTaskSchedulerBuilderBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition();
    beanDefinition.setTargetType(TaskSchedulerBuilder.class);
    beanDefinition.setInstanceSupplier(getTaskSchedulerBuilderInstanceSupplier());
    return beanDefinition;
  }
}
