package fr.carbon.packaging.demo.service;

import fr.carbon.packaging.demo.mapper.TaskMapper;
import fr.carbon.packaging.demo.repository.TaskRepository;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * Bean definitions for {@link TaskService}.
 */
public class TaskService__BeanDefinitions {
  /**
   * Get the bean instance supplier for 'taskService'.
   */
  private static BeanInstanceSupplier<TaskService> getTaskServiceInstanceSupplier() {
    return BeanInstanceSupplier.<TaskService>forConstructor(TaskRepository.class, TaskMapper.class)
            .withGenerator((registeredBean, args) -> new TaskService(args.get(0), args.get(1)));
  }

  /**
   * Get the bean definition for 'taskService'.
   */
  public static BeanDefinition getTaskServiceBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(TaskService.class);
    beanDefinition.setInstanceSupplier(getTaskServiceInstanceSupplier());
    return beanDefinition;
  }
}
