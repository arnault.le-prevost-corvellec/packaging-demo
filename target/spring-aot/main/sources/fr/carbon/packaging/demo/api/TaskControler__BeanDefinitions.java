package fr.carbon.packaging.demo.api;

import fr.carbon.packaging.demo.mapper.TaskMapper;
import fr.carbon.packaging.demo.service.TaskService;
import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * Bean definitions for {@link TaskControler}.
 */
public class TaskControler__BeanDefinitions {
  /**
   * Get the bean instance supplier for 'taskControler'.
   */
  private static BeanInstanceSupplier<TaskControler> getTaskControlerInstanceSupplier() {
    return BeanInstanceSupplier.<TaskControler>forConstructor(TaskService.class, TaskMapper.class)
            .withGenerator((registeredBean, args) -> new TaskControler(args.get(0), args.get(1)));
  }

  /**
   * Get the bean definition for 'taskControler'.
   */
  public static BeanDefinition getTaskControlerBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(TaskControler.class);
    beanDefinition.setInstanceSupplier(getTaskControlerInstanceSupplier());
    return beanDefinition;
  }
}
