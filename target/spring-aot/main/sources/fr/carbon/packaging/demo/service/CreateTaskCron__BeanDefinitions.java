package fr.carbon.packaging.demo.service;

import org.springframework.beans.factory.aot.BeanInstanceSupplier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * Bean definitions for {@link CreateTaskCron}.
 */
public class CreateTaskCron__BeanDefinitions {
  /**
   * Get the bean instance supplier for 'createTaskCron'.
   */
  private static BeanInstanceSupplier<CreateTaskCron> getCreateTaskCronInstanceSupplier() {
    return BeanInstanceSupplier.<CreateTaskCron>forConstructor(TaskService.class)
            .withGenerator((registeredBean, args) -> new CreateTaskCron(args.get(0)));
  }

  /**
   * Get the bean definition for 'createTaskCron'.
   */
  public static BeanDefinition getCreateTaskCronBeanDefinition() {
    RootBeanDefinition beanDefinition = new RootBeanDefinition(CreateTaskCron.class);
    beanDefinition.setInstanceSupplier(getCreateTaskCronInstanceSupplier());
    return beanDefinition;
  }
}
