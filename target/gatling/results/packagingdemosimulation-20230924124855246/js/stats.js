var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "5668",
        "ok": "5665",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "3",
        "ok": "3",
        "ko": "121"
    },
    "maxResponseTime": {
        "total": "1254",
        "ok": "1254",
        "ko": "189"
    },
    "meanResponseTime": {
        "total": "96",
        "ok": "96",
        "ko": "159"
    },
    "standardDeviation": {
        "total": "113",
        "ok": "113",
        "ko": "28"
    },
    "percentiles1": {
        "total": "69",
        "ok": "69",
        "ko": "166"
    },
    "percentiles2": {
        "total": "142",
        "ok": "142",
        "ko": "178"
    },
    "percentiles3": {
        "total": "294",
        "ok": "294",
        "ko": "187"
    },
    "percentiles4": {
        "total": "519",
        "ok": "519",
        "ko": "189"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 5655,
    "percentage": 100
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "62.286",
        "ok": "62.253",
        "ko": "0.033"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "2834",
        "ok": "2831",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "121"
    },
    "maxResponseTime": {
        "total": "1254",
        "ok": "1254",
        "ko": "189"
    },
    "meanResponseTime": {
        "total": "167",
        "ok": "167",
        "ko": "159"
    },
    "standardDeviation": {
        "total": "118",
        "ok": "118",
        "ko": "28"
    },
    "percentiles1": {
        "total": "136",
        "ok": "136",
        "ko": "166"
    },
    "percentiles2": {
        "total": "203",
        "ok": "203",
        "ko": "178"
    },
    "percentiles3": {
        "total": "385",
        "ok": "385",
        "ko": "187"
    },
    "percentiles4": {
        "total": "658",
        "ok": "658",
        "ko": "189"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 2821,
    "percentage": 100
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "31.143",
        "ok": "31.11",
        "ko": "0.033"
    }
}
    },"req_create-task-9c0f2": {
        type: "REQUEST",
        name: "Create task",
path: "Create task",
pathFormatted: "req_create-task-9c0f2",
stats: {
    "name": "Create task",
    "numberOfRequests": {
        "total": "2834",
        "ok": "2834",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "331",
        "ok": "331",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "115",
        "ok": "115",
        "ko": "-"
    },
    "percentiles4": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 2834,
    "percentage": 100
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "31.143",
        "ok": "31.143",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
