var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "966",
        "ok": "941",
        "ko": "25"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "454",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "16405",
        "ok": "16405",
        "ko": "2896"
    },
    "meanResponseTime": {
        "total": "9549",
        "ok": "9765",
        "ko": "1402"
    },
    "standardDeviation": {
        "total": "3308",
        "ok": "3066",
        "ko": "907"
    },
    "percentiles1": {
        "total": "10303",
        "ok": "10367",
        "ko": "1428"
    },
    "percentiles2": {
        "total": "11503",
        "ok": "11632",
        "ko": "1980"
    },
    "percentiles3": {
        "total": "13619",
        "ok": "13622",
        "ko": "2757"
    },
    "percentiles4": {
        "total": "14288",
        "ok": "14328",
        "ko": "2867"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 8,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 927,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 25,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "23",
        "ok": "22.405",
        "ko": "0.595"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "966",
        "ok": "941",
        "ko": "25"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "454",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "16405",
        "ok": "16405",
        "ko": "2896"
    },
    "meanResponseTime": {
        "total": "9549",
        "ok": "9765",
        "ko": "1402"
    },
    "standardDeviation": {
        "total": "3308",
        "ok": "3066",
        "ko": "907"
    },
    "percentiles1": {
        "total": "10303",
        "ok": "10367",
        "ko": "1428"
    },
    "percentiles2": {
        "total": "11503",
        "ok": "11632",
        "ko": "1980"
    },
    "percentiles3": {
        "total": "13619",
        "ok": "13622",
        "ko": "2757"
    },
    "percentiles4": {
        "total": "14288",
        "ok": "14328",
        "ko": "2867"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 8,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 927,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 25,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "23",
        "ok": "22.405",
        "ko": "0.595"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
