var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "1438",
        "ok": "232",
        "ko": "1206"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "8",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "60014",
        "ok": "15837",
        "ko": "60014"
    },
    "meanResponseTime": {
        "total": "29566",
        "ok": "6644",
        "ko": "33975"
    },
    "standardDeviation": {
        "total": "24873",
        "ok": "4287",
        "ko": "24772"
    },
    "percentiles1": {
        "total": "10003",
        "ok": "6308",
        "ko": "15199"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "9936",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "13735",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "15280",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 20,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 206,
    "percentage": 14
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 1206,
    "percentage": 84
},
    "meanNumberOfRequestsPerSecond": {
        "total": "10.574",
        "ok": "1.706",
        "ko": "8.868"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "719",
        "ok": "153",
        "ko": "566"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "225",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "15837",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "26810",
        "ok": "7491",
        "ko": "32033"
    },
    "standardDeviation": {
        "total": "23846",
        "ok": "4375",
        "ko": "24270"
    },
    "percentiles1": {
        "total": "10002",
        "ok": "7013",
        "ko": "10003"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "11656",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "14962",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "15523",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 6,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 144,
    "percentage": 20
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 566,
    "percentage": 79
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.287",
        "ok": "1.125",
        "ko": "4.162"
    }
}
    },"req_create-task-9c0f2": {
        type: "REQUEST",
        name: "Create task",
path: "Create task",
pathFormatted: "req_create-task-9c0f2",
stats: {
    "name": "Create task",
    "numberOfRequests": {
        "total": "719",
        "ok": "79",
        "ko": "640"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "212"
    },
    "maxResponseTime": {
        "total": "60014",
        "ok": "11620",
        "ko": "60014"
    },
    "meanResponseTime": {
        "total": "32321",
        "ok": "5004",
        "ko": "35693"
    },
    "standardDeviation": {
        "total": "25564",
        "ok": "3581",
        "ko": "25082"
    },
    "percentiles1": {
        "total": "10003",
        "ok": "4947",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "7942",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "10815",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "11502",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 14,
    "percentage": 2
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 62,
    "percentage": 9
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 640,
    "percentage": 89
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.287",
        "ok": "0.581",
        "ko": "4.706"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
