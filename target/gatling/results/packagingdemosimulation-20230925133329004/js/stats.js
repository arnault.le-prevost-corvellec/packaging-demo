var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "372",
        "ok": "337",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "4946",
        "ok": "4946",
        "ko": "18792"
    },
    "maxResponseTime": {
        "total": "40566",
        "ok": "40566",
        "ko": "27299"
    },
    "meanResponseTime": {
        "total": "22327",
        "ok": "22304",
        "ko": "22544"
    },
    "standardDeviation": {
        "total": "6440",
        "ok": "6724",
        "ko": "2335"
    },
    "percentiles1": {
        "total": "23781",
        "ok": "24007",
        "ko": "22325"
    },
    "percentiles2": {
        "total": "25879",
        "ok": "26128",
        "ko": "24561"
    },
    "percentiles3": {
        "total": "31655",
        "ok": "31785",
        "ko": "26081"
    },
    "percentiles4": {
        "total": "35597",
        "ok": "35602",
        "ko": "26890"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 337,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 35,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "6.889",
        "ok": "6.241",
        "ko": "0.648"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "372",
        "ok": "337",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "4946",
        "ok": "4946",
        "ko": "18792"
    },
    "maxResponseTime": {
        "total": "40566",
        "ok": "40566",
        "ko": "27299"
    },
    "meanResponseTime": {
        "total": "22327",
        "ok": "22304",
        "ko": "22544"
    },
    "standardDeviation": {
        "total": "6440",
        "ok": "6724",
        "ko": "2335"
    },
    "percentiles1": {
        "total": "23781",
        "ok": "24007",
        "ko": "22325"
    },
    "percentiles2": {
        "total": "25879",
        "ok": "26128",
        "ko": "24561"
    },
    "percentiles3": {
        "total": "31655",
        "ok": "31785",
        "ko": "26081"
    },
    "percentiles4": {
        "total": "35597",
        "ok": "35602",
        "ko": "26890"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 337,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 35,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "6.889",
        "ok": "6.241",
        "ko": "0.648"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
