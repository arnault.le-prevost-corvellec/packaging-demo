var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "1133",
        "ok": "826",
        "ko": "307"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "3887",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "28285",
        "ok": "28285",
        "ko": "19179"
    },
    "meanResponseTime": {
        "total": "15663",
        "ok": "17941",
        "ko": "9534"
    },
    "standardDeviation": {
        "total": "5326",
        "ok": "4255",
        "ko": "2106"
    },
    "percentiles1": {
        "total": "17126",
        "ok": "18605",
        "ko": "10002"
    },
    "percentiles2": {
        "total": "19790",
        "ok": "20552",
        "ko": "10002"
    },
    "percentiles3": {
        "total": "23152",
        "ok": "23877",
        "ko": "10003"
    },
    "percentiles4": {
        "total": "25582",
        "ok": "25892",
        "ko": "16967"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 826,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 307,
    "percentage": 27
},
    "meanNumberOfRequestsPerSecond": {
        "total": "22.66",
        "ok": "16.52",
        "ko": "6.14"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "1133",
        "ok": "826",
        "ko": "307"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "3887",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "28285",
        "ok": "28285",
        "ko": "19179"
    },
    "meanResponseTime": {
        "total": "15663",
        "ok": "17941",
        "ko": "9534"
    },
    "standardDeviation": {
        "total": "5326",
        "ok": "4255",
        "ko": "2106"
    },
    "percentiles1": {
        "total": "17126",
        "ok": "18605",
        "ko": "10002"
    },
    "percentiles2": {
        "total": "19790",
        "ok": "20552",
        "ko": "10002"
    },
    "percentiles3": {
        "total": "23152",
        "ok": "23877",
        "ko": "10003"
    },
    "percentiles4": {
        "total": "25582",
        "ok": "25892",
        "ko": "16967"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 826,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 307,
    "percentage": 27
},
    "meanNumberOfRequestsPerSecond": {
        "total": "22.66",
        "ok": "16.52",
        "ko": "6.14"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
