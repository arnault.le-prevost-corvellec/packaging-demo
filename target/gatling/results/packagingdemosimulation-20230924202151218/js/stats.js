var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "712",
        "ok": "517",
        "ko": "195"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "3233",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "25260",
        "ok": "25260",
        "ko": "17896"
    },
    "meanResponseTime": {
        "total": "12653",
        "ok": "13762",
        "ko": "9712"
    },
    "standardDeviation": {
        "total": "4174",
        "ok": "3670",
        "ko": "3998"
    },
    "percentiles1": {
        "total": "12425",
        "ok": "13346",
        "ko": "9915"
    },
    "percentiles2": {
        "total": "15615",
        "ok": "16134",
        "ko": "12141"
    },
    "percentiles3": {
        "total": "19483",
        "ok": "19992",
        "ko": "15830"
    },
    "percentiles4": {
        "total": "22495",
        "ok": "23881",
        "ko": "17510"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 517,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 195,
    "percentage": 27
},
    "meanNumberOfRequestsPerSecond": {
        "total": "18.737",
        "ok": "13.605",
        "ko": "5.132"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "712",
        "ok": "517",
        "ko": "195"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "3233",
        "ko": "1"
    },
    "maxResponseTime": {
        "total": "25260",
        "ok": "25260",
        "ko": "17896"
    },
    "meanResponseTime": {
        "total": "12653",
        "ok": "13762",
        "ko": "9712"
    },
    "standardDeviation": {
        "total": "4174",
        "ok": "3670",
        "ko": "3998"
    },
    "percentiles1": {
        "total": "12425",
        "ok": "13346",
        "ko": "9915"
    },
    "percentiles2": {
        "total": "15615",
        "ok": "16134",
        "ko": "12141"
    },
    "percentiles3": {
        "total": "19483",
        "ok": "19992",
        "ko": "15830"
    },
    "percentiles4": {
        "total": "22495",
        "ok": "23881",
        "ko": "17510"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 517,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 195,
    "percentage": 27
},
    "meanNumberOfRequestsPerSecond": {
        "total": "18.737",
        "ok": "13.605",
        "ko": "5.132"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
