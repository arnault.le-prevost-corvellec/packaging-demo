var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "429",
        "ok": "429",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "521",
        "ok": "521",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27880",
        "ok": "27880",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18400",
        "ok": "18400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7218",
        "ok": "7218",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22437",
        "ok": "22437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27151",
        "ok": "27151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 3,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 423,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.094",
        "ok": "8.094",
        "ko": "-"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "429",
        "ok": "429",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "521",
        "ok": "521",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27880",
        "ok": "27880",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18400",
        "ok": "18400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7218",
        "ok": "7218",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22437",
        "ok": "22437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27151",
        "ok": "27151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 3,
    "percentage": 1
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 423,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.094",
        "ok": "8.094",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
