var stats = {
    type: "GROUP",
name: "All Requests",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "All Requests",
    "numberOfRequests": {
        "total": "378",
        "ok": "316",
        "ko": "62"
    },
    "minResponseTime": {
        "total": "2567",
        "ok": "2567",
        "ko": "10653"
    },
    "maxResponseTime": {
        "total": "43165",
        "ok": "43165",
        "ko": "25005"
    },
    "meanResponseTime": {
        "total": "19726",
        "ok": "20107",
        "ko": "17788"
    },
    "standardDeviation": {
        "total": "6877",
        "ok": "7213",
        "ko": "4314"
    },
    "percentiles1": {
        "total": "21056",
        "ok": "21579",
        "ko": "17488"
    },
    "percentiles2": {
        "total": "23693",
        "ok": "24406",
        "ko": "21634"
    },
    "percentiles3": {
        "total": "30108",
        "ok": "30351",
        "ko": "24231"
    },
    "percentiles4": {
        "total": "37714",
        "ok": "39300",
        "ko": "24749"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 316,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 62,
    "percentage": 16
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.875",
        "ok": "6.583",
        "ko": "1.292"
    }
},
contents: {
"req_retrieve-tasks-73563": {
        type: "REQUEST",
        name: "Retrieve tasks",
path: "Retrieve tasks",
pathFormatted: "req_retrieve-tasks-73563",
stats: {
    "name": "Retrieve tasks",
    "numberOfRequests": {
        "total": "378",
        "ok": "316",
        "ko": "62"
    },
    "minResponseTime": {
        "total": "2567",
        "ok": "2567",
        "ko": "10653"
    },
    "maxResponseTime": {
        "total": "43165",
        "ok": "43165",
        "ko": "25005"
    },
    "meanResponseTime": {
        "total": "19726",
        "ok": "20107",
        "ko": "17788"
    },
    "standardDeviation": {
        "total": "6877",
        "ok": "7213",
        "ko": "4314"
    },
    "percentiles1": {
        "total": "21056",
        "ok": "21579",
        "ko": "17488"
    },
    "percentiles2": {
        "total": "23693",
        "ok": "24406",
        "ko": "21634"
    },
    "percentiles3": {
        "total": "30108",
        "ok": "30351",
        "ko": "24231"
    },
    "percentiles4": {
        "total": "37714",
        "ok": "39300",
        "ko": "24749"
    },
    "group1": {
    "name": "t < 800 ms",
    "htmlName": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms <= t < 1200 ms",
    "htmlName": "t >= 800 ms <br> t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t >= 1200 ms",
    "htmlName": "t >= 1200 ms",
    "count": 316,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "htmlName": "failed",
    "count": 62,
    "percentage": 16
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.875",
        "ok": "6.583",
        "ko": "1.292"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
