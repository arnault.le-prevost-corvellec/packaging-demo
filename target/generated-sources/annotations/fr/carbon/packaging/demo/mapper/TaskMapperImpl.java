package fr.carbon.packaging.demo.mapper;

import fr.carbon.packaging.demo.api.dto.TaskDto;
import fr.carbon.packaging.demo.repository.TaskEntity;
import fr.carbon.packaging.demo.service.Task;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-09-25T15:18:56+0200",
    comments = "version: 1.5.3.Final, compiler: Eclipse JDT (IDE) 3.35.0.v20230803-1938, environment: Java 17.0.8.1 (Eclipse Adoptium)"
)
@Component
public class TaskMapperImpl implements TaskMapper {

    @Override
    public TaskDto toTaskDto(Task task) {
        if ( task == null ) {
            return null;
        }

        Long id = null;
        String label = null;

        id = task.id();
        label = task.label();

        TaskDto taskDto = new TaskDto( id, label );

        return taskDto;
    }

    @Override
    public Task toTask(TaskEntity task) {
        if ( task == null ) {
            return null;
        }

        long id = 0L;
        String label = null;

        if ( task.getId() != null ) {
            id = task.getId();
        }
        label = task.getLabel();

        Task task1 = new Task( id, label );

        return task1;
    }
}
